# MCP23017 demo with AT90CAN128 and UART client  

Demo program for i2c I/O expander MCP23017. The program configures the PORTA  
to pull-up inputs and send the values to UART. With the client program you can  
see the binary form (bit 7..0) the states of the inputs.

## Compiling  

You need the **avr-gcc** toolchain for compiling the MCU program and **gcc**  
for the client. You can build it with **make** without any parameters. For the  
MCU the Makefile can upload the program with **make flash**.
