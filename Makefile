#!/usr/bin/make -f

NAME = MCP12017
SRC	 = main.c lib/i2c.c lib/uart.c

MCU_FREQ	= 16000000UL
UART_BAUD	= 9600UL
MCU_TARGET	= at90can128
PROGRAMMER_MCU	= c128
PROGRAMMER	= avrisp2

CC	= avr-gcc
OBJCOPY	= avr-objcopy

CFLAGS	+= -mmcu=$(MCU_TARGET) -Os -Wall -DF_CPU=$(MCU_FREQ) -DBAUD=$(UART_BAUD)
LDFLAGS	+= -mmcu=$(MCU_TARGET)
LIBS	+=
OBJ	:= $(patsubst %.c,%.o,$(SRC))

all: $(NAME).hex

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(NAME).elf: $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

clean:
	rm -f $(OBJ)
	rm -f $(NAME).elf
	rm -f $(NAME).hex

flash: $(NAME).hex
	avrdude -v -F -c $(PROGRAMMER) -p $(PROGRAMMER_MCU) -U flash:w:$(NAME).hex
