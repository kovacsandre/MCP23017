#ifndef I2C_H
#define I2C_H

void i2c_init(uint32_t TWI_FREQ);
uint8_t i2c_writebyte(uint8_t dev_addr, uint8_t dev_reg, uint8_t data);
uint8_t i2c_readbyte(uint8_t dev_addr, uint8_t dev_reg, uint8_t *data);

#endif
