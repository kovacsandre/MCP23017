#ifndef UART_H
#define UART_H

void USART0_Init(void);
void USART0_Transmit(uint8_t data);
void USART0_send_msg(uint8_t *msg);

#endif
