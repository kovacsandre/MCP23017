#include <avr/io.h>
#include <util/twi.h>
#include "i2c.h"

#define MAX_TRIES 10

enum op {
    I2C_START,
    I2C_DATA,
    I2C_STOP
};

static uint8_t i2c_transmit(enum op type);

void
i2c_init(uint32_t TWI_FREQ)
{
    /* Init i2c */
    TWSR = 0x00;   // Select Prescaler of 1
    TWBR = ((F_CPU / TWI_FREQ) - 16) / 2;
    TWCR = (1 << TWEN);
}

static uint8_t
i2c_transmit(enum op type)
{
    switch(type) {
        case I2C_START:    // Send Start Condition
            TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
        break;
        case I2C_DATA:     // Send Data
            TWCR = (1 << TWINT) | (1 << TWEN);
        break;
        case I2C_STOP:     // Send Stop Condition
            TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
        default:
            return 0;
    }
    // Wait for TWINT flag set in TWCR Register
    while (!(TWCR & (1 << TWINT)));
    // Return TWI Status Register, mask the prescaler bits (TWPS1,TWPS0)
    return (TWSR & 0xF8);
}

uint8_t
i2c_writebyte(uint8_t dev_addr, uint8_t dev_reg, uint8_t data)
{
    uint8_t n = 0;
    uint8_t twi_status;
    int8_t r_val = -1;

i2c_retry:
    if (n++ >= MAX_TRIES) return r_val;
    // Transmit Start Condition
    twi_status=i2c_transmit(I2C_START);

    // Check the TWI Status
    if (twi_status == TW_MT_ARB_LOST) goto i2c_retry;
    if ((twi_status != TW_START) && (twi_status != TW_REP_START)) goto i2c_quit;

    // Send slave address (SLA_W)
    TWDR = (dev_addr | TW_WRITE);
    // Transmit I2C Data
    twi_status=i2c_transmit(I2C_DATA);
    // Check the TWSR status
    if ((twi_status == TW_MT_SLA_NACK) || (twi_status == TW_MT_ARB_LOST)) goto i2c_retry;
    if (twi_status != TW_MT_SLA_ACK) goto i2c_quit;
    // Send register address
    TWDR = dev_reg;
    // Transmit I2C Data
    twi_status=i2c_transmit(I2C_DATA);
    // Check the TWSR status
    if (twi_status != TW_MT_DATA_ACK) goto i2c_quit;
    // Put data into data register and start transmission
    TWDR = data;
    // Transmit I2C Data
    twi_status=i2c_transmit(I2C_DATA);
    // Check the TWSR status
    if (twi_status != TW_MT_DATA_ACK) goto i2c_quit;
    // TWI Transmit Ok
    r_val=1;

i2c_quit:
    // Transmit I2C Data
    twi_status=i2c_transmit(I2C_STOP);
    return r_val;
}

uint8_t
i2c_readbyte(uint8_t dev_addr, uint8_t dev_reg, uint8_t *data)
{
    uint8_t n = 0;
    uint8_t twi_status;
    int8_t r_val = -1;

i2c_retry:
    if (n++ >= MAX_TRIES) return r_val;
    // Transmit Start Condition
    twi_status=i2c_transmit(I2C_START);

    // Check the TWSR status
    if (twi_status == TW_MT_ARB_LOST) goto i2c_retry;
    if ((twi_status != TW_START) && (twi_status != TW_REP_START)) goto i2c_quit;
    // Send slave address (SLA_W) 0xa0
    TWDR = (dev_addr | TW_WRITE);
    // Transmit I2C Data
    twi_status=i2c_transmit(I2C_DATA);
    // Check the TWSR status
    if ((twi_status == TW_MT_SLA_NACK) || (twi_status == TW_MT_ARB_LOST)) goto i2c_retry;
    if (twi_status != TW_MT_SLA_ACK) goto i2c_quit;
    // Register addressing
    TWDR = dev_reg;
    // Transmit I2C Data
    twi_status=i2c_transmit(I2C_DATA);
    // Check the TWSR status
    if (twi_status != TW_MT_DATA_ACK) goto i2c_quit;

    // Send start Condition
    twi_status=i2c_transmit(I2C_START);
    // Check the TWSR status
    if (twi_status == TW_MT_ARB_LOST) goto i2c_retry;
    if ((twi_status != TW_START) && (twi_status != TW_REP_START)) goto i2c_quit;
    // Send slave address (SLA_R)
    TWDR = (dev_addr | TW_READ);
    // Transmit I2C Data
    twi_status=i2c_transmit(I2C_DATA);

    // Check the TWSR status
    if ((twi_status == TW_MR_SLA_NACK) || (twi_status == TW_MR_ARB_LOST)) goto i2c_retry;
    if (twi_status != TW_MR_SLA_ACK) goto i2c_quit;

    // Get the Data
    twi_status=i2c_transmit(I2C_DATA);
    if ((TWSR & 0xF8) != TW_MR_DATA_NACK) goto i2c_retry;

    *data = TWDR;


i2c_quit:
    // Send Stop Condition
    twi_status=i2c_transmit(I2C_STOP);
    return r_val;
}
