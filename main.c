#include <avr/io.h>
#include <util/delay.h>
#include "lib/i2c.h"
#include "lib/uart.h"
#include "main.h"

#define I2C_ADDR  0x40      // A0 A1 A2 connected to GND

int
main(void)
{
    uint8_t data;

    /* Init UART0 and I2C at 400 kHz */
    USART0_Init();
    i2c_init(400000);

    /* Set IOCON.BANK to zero (see datasheet table 3-4 and 3-5) */
    i2c_writebyte(I2C_ADDR, 0x05, 0b00000000);
    /* Set PortA direction */
    i2c_writebyte(I2C_ADDR, MCP23017_IODIRA, 0xFF);
    /* Set pull-ups for PortA */
    i2c_writebyte(I2C_ADDR, MCP23017_GPPUA, 0xFF);

    for(;;) {
        /* Read PortA data and write to UART0
         * (normally you do this when an interrupt occus from the IC)
         */
        i2c_readbyte(I2C_ADDR, MCP23017_GPIOA, &data);
        USART0_Transmit(data);
        /* 1s Delay */
        _delay_ms(1000);
    }

    return 0;
}
